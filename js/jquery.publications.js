var oTable;
$(function() {
	
	 var oTable = $('table').dataTable( {
	"sDom":'<"publicationsTop container" and <"row" and <"col7" and i><"col10" and l>>><"publicationsMiddle container" and <"row" and <"col24" and w r t>>><"publicationsBottom container" and <"row" and <"col12" and i><"col12 last" and p>>><"clear">',
	//"sDom": '<"top"<"col12" and i><"col12 last" and l>w>rt<"bottom"<"col12" and i><"col12 last" and p>>',
	"aaSorting": [[ 2, "desc" ]]
} );

	$("#pubSearch").keyup( function(){
			 oTable.fnFilter( $(this).val() );
			 //oTable.fnFilter().keyup();
	} );
	
	$('.icon-search-close').each(function() { 
		$(this).click(function(){
			 $("#pubSearch").val('');
			 oTable.fnFilter($("#pubSearch").val());
		});
	});
	
	$("select").each(function() {
		$(this).selectBoxIt();
	});
	
	$('a.originalDocument').qtip({ // Grab some elements to apply the tooltip to
	content: 'Original HEFCE document',
	position: {
			my: 'bottom left',  // Position my top left...
			at: 'top center' // at the bottom right of...
	},
	style: {
		tip: {
						width: 14,
						height: 8,
						corner: true,
					offset: 10
				}
	}
})
	
});
(function($) {
		// if element exists function
		$.fn.exists = function(callback) {
			var args = [].slice.call(arguments, 1);
		
			if (this.length) {
				callback.call(this, args);
			}
		
			return this;
		};
	
	
    'use strict';
    // Sort us out with the options parameters
    var getAnimOpts = function (a, b, c) {
            if (!a) { return {duration: 'normal'}; }
            if (!!c) { return {duration: a, easing: b, complete: c}; }
            if (!!b) { return {duration: a, complete: b}; }
            if (typeof a === 'object') { return a; }
            return { duration: a };
        },
        getUnqueuedOpts = function (opts) {
            return {
                queue: false,
                duration: opts.duration,
                easing: opts.easing
            };
        };
    // Declare our new effects
    $.fn.showDown = function (a, b, c) {
        var slideOpts = getAnimOpts(a, b, c), fadeOpts = getUnqueuedOpts(slideOpts);
        $(this).hide().css('opacity', 0).slideDown(slideOpts).animate({ opacity: 1 }, fadeOpts);
    };
    $.fn.hideUp = function (a, b, c) {
        var slideOpts = getAnimOpts(a, b, c), fadeOpts = getUnqueuedOpts(slideOpts);
        $(this).show().css('opacity', 1).slideUp(slideOpts).animate({ opacity: 0 }, fadeOpts);
    };
		
		
		// Table styling
		
		$('table tr').each(function(){		
			$(this).find('td:last, th:last').addClass('lastTD');
			$(this).find('td:first, th:first').addClass('firstTD');
		});
		
		// Social Sharing links 
		$(".socialShare .share").each(function() {
			$(this).jqSocialSharer();
		});
		
		jQuery.fn.toggleText = function (value1, value2) {
				return this.each(function () {
						var $this = $(this),
								text = $this.html();
		 
						if (text.indexOf(value1) > -1)
								$this.html(text.replace(value1, value2));
						else
								$this.html(text.replace(value2, value1));
				});
		};
		
		$.fn.slideFadeToggle  = function(speed, easing, callback) {
        return this.animate({opacity: 'toggle', height: 'toggle'}, speed, easing, callback);
		};
		
		$('.simpleTabs .tab .tabTrigger').each(function() {
			$(this).tap(50, function() {		
				$(this).next('.tabContent').slideFadeToggle();
				$(this).toggleText('<i class="icon-blue-plus"></i>', '<i class="icon-blue-minus"></i>');
			});
		});
		
		$('#heroHighlight .expansionTrigger, #heroImage .expansionTrigger').each(function() {
			$(this).tap(50, function() {		
				var highlight = $('#heroHighlight');
				highlight.slideFadeToggle(function() {
					
						$('html, body').animate({
							scrollTop : highlight.offset().top
						}, 800);
				});
				$('#heroImage .expansionTrigger').toggleText('<i class="icon-blue-arrow-down"></i>', '<i class="icon-blue-arrow-up"></i>');
			});
		});
		
		$('#mobileMenu .menuTrigger').tap(50, function() {
			$('#mainNavContainer').slideFadeToggle();		
		});
				
}(jQuery));

/**
 * stacktable.js
 * Author & copyright (c) 2012: John Polacek
 * Dual MIT & GPL license
 *
 * Page: http://johnpolacek.github.com/stacktable.js
 * Repo: https://github.com/johnpolacek/stacktable.js/
 *
 * jQuery plugin for stacking tables on small screens
 *
 */
;(function($) {

  $.fn.stacktable = function(options) {
    var $tables = this,
        defaults = {id:'stacktable',hideOriginal:false},
        settings = $.extend({}, defaults, options);

    return $tables.each(function() {
      var $stacktable = $('<table class="'+settings.id+'"><tbody></tbody></table>');
      if (typeof settings.myClass !== undefined) $stacktable.addClass(settings.myClass);
      var markup = '';
      $table = $(this);
      $topRow = $table.find('tr').eq(0);
      $table.find('tr').each(function(index,value) {
        markup += '<tr>';
        // for the first row, top left table cell is the head of the table
        if (index===0) {
          markup += '<tr><th class="st-head-row st-head-row-main" colspan="2">'+$(this).find('th,td').eq(0).html()+'</th></tr>';
        }
        // for the other rows, put the left table cell as the head for that row
        // then iterate through the key/values
        else {
          $(this).find('td').each(function(index,value) {
            if (index===0) {
              markup += '<tr><th class="st-head-row" colspan="2">'+$(this).html()+'</th></tr>';
            } else {
              if ($(this).html() !== ''){
                markup += '<tr>';
                if ($topRow.find('td,th').eq(index).html()){
                  markup += '<td class="st-key">'+$topRow.find('td,th').eq(index).html()+'</td>';
                } else {
                  markup += '<td class="st-key"></td>';
                }
                markup += '<td class="st-val">'+$(this).html()+'</td>';
                markup += '</tr>';
              }
            }
          });
        }
      });
      $stacktable.append($(markup));
      $table.before($stacktable);
      if (settings.hideOriginal) $table.hide();
    });
  };

}(jQuery));